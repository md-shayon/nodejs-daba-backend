const jwt = require('jsonwebtoken');
const { User } = require('../models');

const ensureAuth = async (req, res, next) => {
  try {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const decodedData = jwt.verify(token, process.env.JWT_SECRET);
      const findUser = await User.findOne({ where: { id: decodedData.id } });
      // console.log({findUser});
      if (findUser) {
        req.userId = decodedData?.id;
        req.userRole = decodedData.role;
        return next();
      }
      return res.status(401).json({ msg: 'Unauthorized - Invalid token' });
    }
    return res.status(401).json({ msg: 'Unauthorized - no token found' });
  } catch (error) {
    return res.status(401).json({ msg: 'Unauthorized', error });
  }
};

const ensureGuest = async (req, res, next) => {
  try {
    // console.log(req.headers);
    // console.log(req.body);
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const decodedData = jwt.verify(token, process.env.JWT_SECRET);
      req.userId = decodedData?.id;
      req.userRole = decodedData.role;
      return res.status(401).json({ msg: 'Authorizedtoken found' });
    }
    return next();
  } catch (error) {
    // console.log(error);
    return res.status(401).json({ msg: 'Unauthorized', error });
  }
};

module.exports = { ensureAuth, ensureGuest };
