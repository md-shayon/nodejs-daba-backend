const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Router } = require('express');
const { User } = require('../models');
const { GENERAL, ADMIN } = require('../config/keys').role;
const { ensureAuth } = require('../middleware/auth');

const router = Router();

/**
 * @route for user signup or register
 */
//  curl --location --request POST 'http://127.0.0.1:5000/api/user/signup' --header 'Content-Type: application/json' --data-raw '{"name": "Jaden Upwork", "role": "ADMIN", "email": "jadenrodabaugh@kinoverse.net", "password": "Jaden1234"}'
router.post('/signup', async (req, res) => {
  const role = GENERAL;
  const { email, password, name } = req.body;

  // return res.status(406).json({ msg: "Invalid password" });

  try {
    const userExist = await User.findOne({ where: { email } });
    if (userExist !== null)
      return res.status(208).json({ msg: 'User already exists' });

    // // console.log(password);
    const hashedPassword = await bcrypt.hash(password, 10);
    // console.log("hashed password - ", hashedPassword);
    const user = await User.create({
      email,
      password: hashedPassword,
      name,
      role,
    });

    const token = jwt.sign(
      {
        email: user.dataValues.email,
        id: user.dataValues.id,
        role: user.dataValues.role,
      },
      process.env.JWT_SECRET,
      {
        expiresIn: '1h',
      }
    );

    return res.status(201).json({ user: user.dataValues, token });
  } catch (error) {
    // console.log(error);
    return res
      .status(500)
      .json({ msg: 'Something went wrong', error: error.message });
  }
});

/**
 * @route for user signin or login
 */
router.post('/signin', async (req, res) => {
  const { email, password } = req.body;

  try {
    const userExist = await User.findOne({ where: { email } });
    if (!userExist) return res.status(404).json({ msg: "User doesn't exist" });
    // console.log(userExist);
    const isPasswordCorrect = await bcrypt.compare(
      password,
      userExist.dataValues.password
    );
    if (!isPasswordCorrect)
      return res.status(406).json({ msg: 'Invalid credentials' });
    const token = jwt.sign(
      {
        email: userExist.email,
        id: userExist.id,
        role: userExist.role,
      },
      process.env.JWT_SECRET,
      {
        expiresIn: '1d',
      }
    );
    const newUser = {
      name: userExist.name,
      email: userExist.email,
      role: userExist.role,
      id: userExist.id,
    };
    return res.status(200).json({ user: newUser, token });
  } catch (err) {
    // console.log(err);
    return res.status(500).json({ msg: 'Something went wrong' });
  }
});


router.get('/appropriate-user', ensureAuth, async (req, res) => {
  if (req.userRole === GENERAL) {
    // send single user
    const customers = await User.findOne({ where: { id: req.userId } });
    // console.log({customers});
    const { name, id, role, email } = customers.dataValues;
    return res.status(200).json({
      msg: 'This user is authenticated',
      userRole: req.userRole,
      user: [{ name, id, role, email }],
    });
  }
  if (req.userRole === ADMIN) {
    // send list of user
    const allUsers = await User.findAll();
    const userPrecise = allUsers.map((user) => ({
      // services: user.services,
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    }));

    return res.status(200).json({
      msg: 'Get all users',
      userRole: req.userRole,
      user: userPrecise,
    });
  }
  return res.status(401).json({ msg: 'This user is not authenticated' });
});



module.exports = router;
