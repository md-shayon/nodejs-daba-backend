const express = require('express');
const { User, BuyService, Service, Guest } = require('../models');
const { GENERAL, ADMIN } = require('../config/keys').role;
const { ensureAuth } = require('../middleware/auth');

const router = express.Router();

async function preciseUserOrGuest(allServiceHolder, guest) {
  const allBuyServices = [];
  for (let i = 0; i < allServiceHolder.length; i += 1) {
    const serviceHolderService = allServiceHolder[i].getBuyServices();
    allBuyServices.push(serviceHolderService);
  }
  const buyServices = await Promise.all(allBuyServices);
  const buyServiceWithServiceHolderId = [];
  for (let i = 0; i < buyServices.length; i += 1) {
    if (buyServices[i].length > 0) {
      for (let j = 0; j < buyServices[i].length; j += 1) {
        if (guest) {
          buyServiceWithServiceHolderId.push({
            guestId: buyServices[i][j].guest_id,
            purchase: buyServices[i][j].dataValues,
          });
        } else {
          buyServiceWithServiceHolderId.push({
            userId: buyServices[i][j].user_id,
            purchase: buyServices[i][j].dataValues,
          });
        }
      }
    }
  }

  return buyServiceWithServiceHolderId;
}

router.get('/all-services', async (req, res) => {
  try {
    const services = await Service.findAll();
    // console.log({services});
    res.status(200).json({ msg: 'Getting all services', services });
  } catch (error) {
    console.log(error);
  }
});

// appropriate-user -> user-with-services
router.get('/user-with-services', ensureAuth, async (req, res) => {
  // One user one or more service
  if (req.userRole === GENERAL) {
    // send single user
    // const customers = await User.findOne({ where: { id: req.userId } });
    const findBuyService = await BuyService.findOne({
      where: { user_id: req.userId },
    });
    console.log({ findBuyService });
    if (findBuyService === null)
      return res.status(200).json({
        msg: 'User did not purchase any service from us',
        userRole: req.userRole,
        customer: null,
        service: null,
      });
    const service = await findBuyService.getService();
    const customer = await findBuyService.getUser();
    return res.status(200).json({
      msg: 'This user is authenticated',
      userRole: req.userRole,
      customer,
      service,
    });
  }
  if (req.userRole === ADMIN) {
    // send list of user
    // const oneUser = await User.findOne({where: {id: 12}});
    // const userServices = await oneUser.getBuyServices();

    /*
    const allPurchaseServices = await BuyService.findAll();
    const services = [];
    for (let i = 0; i < allPurchaseServices.length; i += 1) {
      const service = await allPurchaseServices[i].getService();
      // console.log({user: allPurchaseServices[i].user});
      if (allPurchaseServices[i].user_id) {
        const serviceUser = await allPurchaseServices[i].getUser();
        services.push({
          service,
          user: serviceUser,
          guest: null,
        });
      }
      if (allPurchaseServices[i].guest_id) {
        const serviceGuest = await allPurchaseServices[i].getGuest();
        services.push({
          service,
          guest: serviceGuest,
          user: null,
        });
      }
    }
    */

    // const fetchServices = await Promise.all(services);

    /**
     * @user
     * Find appropriate user with appropriate service
     */
    const allUsers = await User.findAll();

    const buyServiceWithUserId = await preciseUserOrGuest(allUsers, false);
    const userPrecise = allUsers
      .map((user) => {
        const servicesOfUser = buyServiceWithUserId.filter(
          (bswui) => bswui.userId === user.id
        );
        const preciseServicesOfUser = servicesOfUser.map((sou) => sou.purchase);
        return {
          id: user.id,
          name: user.name,
          email: user.email,
          role: user.role,
          services: preciseServicesOfUser,
          createdAt: user.createdAt,
          updatedAt: user.updatedAt,
        };
      })
      .filter((pu) => pu.role !== ADMIN);

    /**
     * @guest
     * Find appropriate guest with appropriate service
     */
    const allGuests = await Guest.findAll();
    const buyServiceWithGuestId = await preciseUserOrGuest(allGuests, true);
    const guestPrecise = allGuests.map((guest) => {
      const servicesOfGuest = buyServiceWithGuestId.filter(
        (bswui) => bswui.guestId === guest.id
      );
      const preciseServicesOfGuest = servicesOfGuest.map((sou) => sou.purchase);
      return {
        id: guest.id,
        name: guest.name,
        email: guest.email,
        phone: guest.phone,
        services: preciseServicesOfGuest,
        createdAt: guest.createdAt,
        updatedAt: guest.updatedAt,
      };
    });

    return res.status(200).json({
      msg: 'Get all users',
      userRole: req.userRole,
      users: userPrecise,
      guests: guestPrecise,
      // allPurchaseServices,
    });
  }
  return res.status(401).json({ msg: 'This user is not authenticated' });
});

/*
router.post('/create-services', async (req, res) => {
  try {
    const services = Service.bulkCreate([
      {
        name: 'Single Package',
        price: 10,
      },
      {
        name: 'Asset Consulting',
        price: 89,
      },
    ]);
    res.status(201).json({ msg: 'Created two user successfully', services });
  } catch (error) {
    console.log(error);
  }
});
*/

module.exports = router;
