const express = require('express');
const jwt = require('jsonwebtoken');
// This is a public sample test API key.
// Don’t submit any personally identifiable information in requests made with this key.
// Sign in to see your own test API key embedded in code samples.
const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);
const { Service, User, BuyService, Guest } = require('../models');
const { GENERAL, GUEST } = require('../config/keys').role;
const sendEmail = require('../utils/sendEmail');

const router = express.Router();

/**
 * @payment process
 * Payment any service in 3 step
 * 1. Create payment intent @param - serviceId, amount, payment metnod, currency, description
 * 2. Update payment intent metadata with essential info @param - tos, customer info, paymentId
 * 3. Confirm payment intent and save to db @param // paymentId
 */
//  array of the services that they will purchase
router.post('/service-purchase', async (req, res) => {
  const { serviceId } = req.body;
  // console.log(serviceId);

  // console.log(item);
  try {
    const service = await Service.findOne({ where: { id: serviceId } });
    if (service) {
      // Create a PaymentIntent with the order amount and currency
      const paymentIntent = await stripe.paymentIntents.create({
        amount: 100 * service.price,
        currency: 'usd',
        payment_method_types: ['card'],
        // customer: null // Set customer or guest
        description: `purchase a service of ${service.name}`,
        metadata: { service_id: service.id },
        // automatic_payment_methods: {
        //   enabled: true,
        // },
      });

      // console.log(paymentIntent);

      res.status(201).json({
        msg: 'Initialize a payment intent',
        clientSecret: paymentIntent.client_secret,
        paymentIntentID: paymentIntent.id,
      });
    } else {
      res.status(406).json({ msg: 'Incorrect service' });
    }
  } catch (error) {
    console.log(error);
  }
});

// eslint-disable-next-line consistent-return
router.put('/update-payment/:paymentId', async (req, res) => {
  const { tos, customerInfo, selectedAsset } = req.body;
  try {
    if (tos === false)
      return res.status(406).json({ msg: 'You must agree with our TOS!' });

    // console.log({ customerInfo });

    if (customerInfo.type === GENERAL && customerInfo.token === null)
      return res.status(406).json({
        msg: 'If you are our logged in user you must send request with valid login token!',
      });

    const paymentIntent = await stripe.paymentIntents.update(
      req.params.paymentId,
      {
        metadata: {
          tos,
          customerInfo: JSON.stringify(customerInfo),
          selectedAsset: JSON.stringify(selectedAsset),
        },
      }
    );
    // console.log({paymentIntent}); // metadata: { service_id: '1', tos: 'true' }

    res.status(200).json({
      msg: 'Updated a payment intent',
      clientSecret: paymentIntent.client_secret,
      paymentIntentID: paymentIntent.id,
    });
  } catch (error) {
    console.log(error);
    // throw error;
  }
});

// eslint-disable-next-line consistent-return
router.post('/confirm-payment/:paymentId', async (req, res) => {
  // const { clientSecret, paymentIntentId } = req.body;
  const { paymentId } = req.params;
  const paymentIntent = await stripe.paymentIntents.retrieve(paymentId);
  // console.log(paymentIntent);
  // console.log(paymentIntent.metadata.service_id);
  // metadata: { service_id: '1' }
  if (paymentIntent.status === 'succeeded') {
    // Update or add user database // save service and payment id to the db
    let { customerInfo } = paymentIntent.metadata;
    customerInfo = JSON.parse(customerInfo);
    if (customerInfo.phone)
      customerInfo.phone = parseInt(customerInfo.phone, 10);
    const userSelectedAsset = JSON.parse(paymentIntent.metadata.selectedAsset);
    // console.log({userSelectedAsset});
    const findService = await Service.findOne({
      where: { id: paymentIntent.metadata.service_id },
    });
    // console.log({ customerInfo });
    const invoiceURL = `${process.env.FRONTEND_URL}/payment.html?payment_intent=${paymentId}&payment_intent_client_secret=${paymentIntent.client_secret}&redirect_status=${paymentIntent.status}`;
    sendEmail(
      [process.env.ADMIN_EMAIL],
      'Daba: A service sold out',
      `A user purchase our service of ${findService.dataValues.name
        .toString()
        .toLowerCase()}`,
      `<div>
          <h2>Details of service sold.</h2>
          <p>Customer email: ${customerInfo.email} </p>
          <p>Customer description: ${customerInfo.desc}</p>
          <hr />
          <p>Service Name: ${findService.dataValues.name}</p>
          <p>Service ID: ${findService.dataValues.id}</p>
          <p>Service Price: ${findService.dataValues.price}</p>
          <hr />
          <p>Selected Asset: ${userSelectedAsset.name}</p>
        </div>`
    );


    /**
     * @param
     * Check who is perchasing our service, either it is a guest or a user
     */
    if (customerInfo.type === GENERAL) {
      const decodedCustomer = jwt.verify(
        customerInfo.token,
        process.env.JWT_SECRET
      );
      const userId = decodedCustomer.id;
      const findCustomer = await User.findOne({ where: { id: userId } });
      sendEmail(
        [decodedCustomer.email],
        'Daba: A service is been purchased',
        `You have purchase our service of ${findService.dataValues.name
          .toString()
          .toLowerCase()}`,
        `<div>
            <h2>Order details</h2>
            <p>Customer email: ${customerInfo.email} </p>
            <p>Service Name: ${findService.dataValues.name} </p>
            <p>Service ID: ${findService.dataValues.id} </p>
            <p>Price: ${findService.dataValues.price} </p>
            <p>Payment ID: ${paymentId} </p>
            <hr />
            <p>Selected Asset: ${userSelectedAsset.name}</p>
            <a href="${invoiceURL}">Download invoice</a>
          </div>`
      );
      // must add payment id and email
      // console.log({ findCustomer, findService});
      const alreadyPurchase = await BuyService.findOne({
        where: {
          customerEmail: findCustomer.dataValues.email,
          service_id: findService.dataValues.id,
          user_id: findCustomer.dataValues.id,
        },
        // Populate works well - not necessary in this case
        /*
        include: [
          { model: User, as: 'User', attributes: ['id'] },
          { model: Service, as: 'Service', attributes: ['id'] },
        ],
        */
      });
      if (alreadyPurchase)
        return res.status(208).json({
          msg: 'You already purchased our service',
          buyService: alreadyPurchase.dataValues,
          customer: findCustomer,
          service: findService,
        });
      const buyService = await BuyService.create({
        customerEmail: findCustomer.dataValues.email,
        paymentId,
        selectedAsset: userSelectedAsset.id
      });
      await buyService.setService(findService);
      await buyService.setUser(findCustomer);
      await findCustomer.setBuyServices(buyService);
      // console.log({ buyService });
      res.status(201).json({
        msg: 'User purchase a service',
        buyService,
        customer: decodedCustomer,
        service: findService,
      });
      // console.log({ customerService });
    } else if (customerInfo.type === GUEST) {
      // console.log('Guest payment'); // save guest informations
      sendEmail(
        [customerInfo.email],
        'Daba: A service is been purchased',
        `You have purchase our service of ${findService.dataValues.name
          .toString()
          .toLowerCase()}`,
        `<div>
            <h2>Order details</h2>
            <p>Customer email: ${customerInfo.email} </p>
            <p>Service Name: ${findService.dataValues.name} </p>
            <p>Service ID: ${findService.dataValues.id} </p>
            <p>Price: ${findService.dataValues.price} </p>
            <p>Payment ID: ${paymentId} </p>
            <hr />
            <p>Selected Asset: ${userSelectedAsset.name}</p>
            <a href="${invoiceURL}">Download invoice</a>
          </div>`
      );
      // Create a user with guest credentials
      // Send invoice as PDF / email
      const findGuest = await Guest.findOne({
        where: { email: customerInfo.email },
      });
      // console.log({ alreadyPurchase });
      if (findGuest) {
        const alreadyPurchase = await BuyService.findOne({
          where: {
            customerEmail: findGuest.dataValues.email,
            service_id: findService.dataValues.id,
            guest_id: findGuest.dataValues.id,
          },
          // Populate works well - not necessary in this case
          /*
          include: [
            { model: Guest, as: 'Guest', attributes: ['id'] },
            { model: Service, as: 'Service', attributes: ['id'] },
          ],
          */
        });
        if (alreadyPurchase)
          return res.status(208).json({
            msg: 'You already purchased our service',
            buyService: alreadyPurchase.dataValues,
            customer: findGuest,
            service: findService,
          });
        // must add payment id and email
        // console.log({ findCustomer, findService});
        const buyService = await BuyService.create({
          customerEmail: findGuest.dataValues.email,
          paymentId,
          selectedAsset: userSelectedAsset.id
        });
        await buyService.setService(findService);
        await buyService.setGuest(findGuest);
        await findGuest.setBuyServices(buyService);
        res.status(201).json({
          msg: 'Guest found and he purchase a service',
          buyService,
          customer: findGuest,
          service: findService,
        });
      } else {
        // console.log({ customerInfo });
        const createGuest = await Guest.create({
          name: customerInfo.name,
          phone: customerInfo.phone,
          email: customerInfo.email,
        });
        const buyService = await BuyService.create({
          customerEmail: customerInfo.email,
          paymentId,
          selectedAsset: userSelectedAsset.id
        });
        await buyService.setService(findService);
        await buyService.setGuest(createGuest);
        res.status(201).json({
          msg: 'Guest created and he purchase a service',
          buyService,
          customer: createGuest,
          service: findService,
        });
      }
    }
    // console.log(paymentIntent);
  } else {
    // Failed payment
    return res.status(402).json({ msg: 'payment confirmation is not succeed' });
  }
  // GET PAYMENT STATUS
  // const service = getSpecificService(item);
  // Save service for the user
  // res.status(200).json({ msg: 'Success' });
});

module.exports = router;
