const { Router } = require('express');
const { User, Contact } = require('../models');
const sendEmail = require('../utils/sendEmail');

const router = Router();

router.get('/test', async (req, res) => {
  res.status(200).json({ msg: 'success' });
});

router.post('/contact', async (req, res) => {
  // console.log({
  //   ADMIN_EMAIL_HOST: process.env.ADMIN_EMAIL_HOST,
  //   ADMIN_EMAIL: process.env.ADMIN_EMAIL,
  //   ADMIN_PASSWORD: process.env.ADMIN_PASSWORD
  // });
  try {
    const { email, message } = req.body;
    const userExist = await User.findOne({ where: { email } });
    const contact = await Contact.create(req.body);
    if (userExist) {
      await contact.setUser(userExist);
    }
    sendEmail(
      [process.env.ADMIN_EMAIL],
      'Message from daba user',
      'A person tried to contact you from daba',
      `<h2>Message</h2><p>${message}</p>`
    );
    res.status(201).json({ msg: 'send contact info successfully', contact });
  } catch (error) {
    console.log(error);
    res.status(401).json({ msg: 'invalid request' });
  }
});

module.exports = router;
