'use strict';

const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Service extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // Service.belongsToMany(models.User, {
      //   as: 'users',
      //   through: 'UserServices',
      //   foreignKey: 'serviceIds',
      // });
      /**
       * @for one to one relationship
       * Here Service is the parent of BuyService and BuyService is the child
       * This will create a column in BuyService table with name of serviceId
       * Service.hasOne(models.BuyService, { foreignKey: 'serviceId' });
       * Now we can use Service.setBuyService, Service.getBuyService, Service.createBuyService
       * { onDelete: 'CASCADE' } Means if we delete parent record it will delete all child associated with it
       * Note: Also { onDelete: 'CASCADE' } use this in child's belongsTo
       * Has one is exclusive to one to one relationship but belongsto can be used in one to many relationship
       */
      /**
       * @for One to many relationship
       * Service parent and buy service child table
       */

      Service.hasMany(models.BuyService, {
        // onDelete: 'CASCADE',
        foreignKey: 'service_id',
      });
    }
  }
  Service.init(
    {
      price: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'Service',
      freezeTableName: true,
    }
  );
  return Service;
};
