const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class BuyService extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // BuyService.hasOne(models.Service, {
      //   // foreignKey: 'serviceId',
      //   as: 'HomeTeam',
      //   sourceKey: 'id'
      // });
      // Team.hasOne(Game, { as: 'HomeTeam', foreignKey: 'homeTeamId' });
      // Foo.hasOne(Bar, { sourceKey: 'name', foreignKey: 'fooName' });
      /**
       * @for one to one relationship
       * This does exact same as Service.hasOne(BuyService), but to fetch buyservice with service data and also fetch service with buyservice data
       * Here Service is the parent of BuyService and BuyService is the child
       * This will create a column in BuyService table with name of serviceId
       * BuyService.BelongsTo(models.Service, { foreignKey: 'serviceId' });
       * Now we can use Service.setBuyService, Service.getBuyService, Service.createBuyService
       * And also ButService.setService, ButService.getService, ButService.createService
       * Has one is exclusive to one to one relationship but belongsto can be used in one to many relationship
       */
      // Has one user or guest
      BuyService.belongsTo(models.Service, {
        // onDelete: 'CASCADE',
        // foreignKey: 'serviceIds',
        foreignKey: 'service_id',
      });
      BuyService.belongsTo(models.Guest, { foreignKey: 'guest_id' });
      BuyService.belongsTo(models.User, { foreignKey: 'user_id' });
      /*
      BuyService.hasOne(models.User, {
        foreignKey: 'UserId',
      }); // Create a column in  user table with name of UserId
      */
    }
  }
  BuyService.init(
    {
      customerEmail: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      paymentId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      selectedAsset: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'BuyService',
      freezeTableName: true,
    }
  );
  return BuyService;
};
