/* eslint-disable no-undef */
/* eslint-disable node/no-unpublished-require */
const request = require('supertest');
const app = require('../server');

/**
 * @route test from /routes/user.js
 */
describe('User API', () => {
  test('should create a new user', async () => {
    const res = await request(app)
      .post('/api/user/signup')
      .send({
        name: 'Bob Doe',
        email: 'bob@doe.com',
        role: 'GENERAL',
        password: '12345678',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201);
    expect(res.statusCode).toEqual(201);
    expect(res.body.user).not.toBe(null);
    expect(res.body.token).not.toBe(null);
  });

  test('should login user', async () => {
    const res = await request(app)
      .post('/api/user/signin')
      .send({
        email: 'bob@doe.com',
        password: '12345678',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200);
    expect(res.statusCode).toEqual(200);
    expect(res.body.user).not.toBe(null);
    expect(res.body.token).not.toBe(null);
  });
});

/**
 * @route test from /routes/payment.js
 */
describe('Payment API for create payment intent', () => {
  test('should create payment intent', async () => {
    const res = await request(app)
      .post('/api/payment/services')
      .send({
        item: 'ASSET_CONSULTING',
      })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201);
    expect(res.statusCode).toEqual(201);
    expect(res.body.clientSecret).not.toBe(null);
  });


  // test('Payment API for confirm payment', async () => {
  //   const res = await request(app)
  //     .post('/api/payment/confirm-payment')
  //     .send({
  //       clientSecret: 'sjshfsshdshdjshdsjhdsd', 
  //       paymentIntentId: 'pi_734u3h43g34y3t43h4h4g4'
  //     })
  //     .set('Accept', 'application/json')
  //     .expect('Content-Type', /json/);
  //   expect(res.statusCode).toEqual(200);
  //   expect(res.body.paymentIntent).not.toBe(null);
  // });

});
