const dotenv = require('dotenv');

// console.log({node_env: process.env.NODE_ENV});
if (process.env.NODE_ENV === 'development') {
  dotenv.config({ path: './config/.env.dev' });
} else {
  dotenv.config({ path: './config/.env' });
}
const cors = require('cors');
const express = require('express');
const logger = require('morgan');
const db = require('./models');
const indexRoute = require('./routes/index');
const userRoute = require('./routes/user');
const paymentRoute = require('./routes/payment');
const serviceRoute = require('./routes/service');

const PORT = process.env.PORT || 5000;
const app = express();

/**
 * @middleware
 */
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger('dev'));
app.use(cors());

/**
 * @route
 */
app.use('/api', indexRoute);
app.use('/api/user', userRoute);
app.use('/api/payment', paymentRoute);
app.use('/api/service', serviceRoute);


app.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});


// app.listen(PORT, () => console.log(`Listening on port: ${PORT}`));
/*
// sequelize.sync({alter: true, force: true});
*/

// db.sequelize.sync({ alter: true, force: true }).then(() => {
//   app.listen(PORT, () => {
//     console.log(`Server is running on ${PORT}`);
//   });
// });


module.exports = app;
