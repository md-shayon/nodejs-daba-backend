/* eslint-disable no-unused-vars */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    /*
    return queryInterface.sequelize.transaction((t) =>
      Promise.all([
        queryInterface.addColumn(
          'UserServices',
          'userIds',
          { type: Sequelize.DataTypes.INTEGER },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'UserServices',
          'serviceIds',
          { type: Sequelize.DataTypes.INTEGER },
          { transaction: t }
        ),
      ])
    );
    */
    // Async / await
    /*
     queryInterface.addColumn('users', 'region_id',
            {
              type: Sequelize.UUID,
              references: {
                model: 'regions',
                key: 'id',
              },
              onUpdate: 'CASCADE',
              onDelete: 'SET NULL',
              defaultValue: null, after: 'can_maintain_system'
            }),
    */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      /*
      await queryInterface.addColumn(
        'Guest',
        'name',
        {
          type: Sequelize.STRING,
          allowNull: false,
        },
        { transaction }
      );
      */
      /*
      await queryInterface.addColumn(
        'BuyService',
        'serviceId',
        {
          type: Sequelize.UUID,
          references: {
            model: 'Service',
            key: 'id',
          },
          // onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          defaultValue: null,
          // after: 'can_maintain_system',
        },
        { transaction }
      );
      */
      /*
      await queryInterface.addColumn(
        'BuyService',
        'guestId',
        {
          type: Sequelize.UUID,
          references: {
            model: 'Guest',
            key: 'id',
          },
          // onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          defaultValue: null,
          // after: 'can_maintain_system',
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'BuyService',
        'userId',
        {
          type: Sequelize.UUID,
          references: {
            model: 'User',
            key: 'id',
          },
          // onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
          defaultValue: null,
          // after: 'can_maintain_system',
        },
        { transaction }
      );
      */
      await queryInterface.addColumn(
        'BuyService',
        'selectedAsset',
        {
          type: Sequelize.INTEGER,
          allowNull: false,
        },
        { transaction }
      );
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    // await queryInterface.dropTable('purchasedServices');
    // await queryInterface.dropTable('Service');
    // Promise
    /*
    return queryInterface.sequelize.transaction((t) =>
      Promise.all([
        queryInterface.removeColumn('UserServices', 'userId', { transaction: t }),
        queryInterface.removeColumn('UserServices', 'serviceId', { transaction: t }),
      ])
    );
    */
    /*
    // async / await
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('BuyService', 'user', {
        transaction,
      });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
    */
    /*
    // async / await
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.removeColumn('Guest', 'phone', {
        transaction,
      });

      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
    */
  },
};
