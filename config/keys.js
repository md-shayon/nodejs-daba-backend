module.exports = {
  role: {
    SUPER: 'SUPER',
    GENERAL: 'GENERAL',
    ADMIN: 'ADMIN',
    GUEST: 'GUEST',
  },
  services: [
    {
      id: 1,
      serviceType: 'ASSET_CONSULTING',
      price: 100 * 10,
    },
    {
      id: 2,
      serviceType: 'SINGLE_PACKAGE',
      price: 100 * 89,
    },
  ],
};
