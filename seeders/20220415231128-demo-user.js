'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'User',
      [
        {
          name: 'Cristiano Ronaldo',
          role: 'ADMIN',
          password: 'Ronaldo1234',
          email: 'ronaldo@mutd.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Lionel Messi',
          role: 'ADMIN',
          password: 'Messi1234',
          email: 'messi@psg.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('User', null, {});
  },
};
