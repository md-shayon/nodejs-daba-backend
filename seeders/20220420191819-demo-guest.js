'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'Guest',
      [
        {
          name: 'Jadon Sancho',
          phone: 12328632,
          email: 'sancho@mutd.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Marcos Rashford',
          phone: 1232834632,
          email: 'marcos@mutd.com',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
